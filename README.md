Apollo2
=======================================
AWS function written using Java Micronaut framework

# Pre-requisites
* Install Docker
  * Follow this to run without root user: https://docs.docker.com/engine/install/linux-postinstall/
* Install Homebrew
* Install AWS CLI 2
* Install AWS SAM CLI

# Sample Commands
* mn create-function-app -b=gradle -i -l=groovy -t=spock --jdk=11 apollo2
* sam init --runtime java11 --dependency-manager gradle --app-template hello-world --name apollo2
* To test the lambda function locally: `sam local invoke -e event.json BookFunction`
* To test the lambda function locally: `sam local invoke -e eventAuth.json AuthFunction`
* jar -t -f <filename>

# References
* https://www.vogella.com/tutorials/GradleTutorial/article.html
* https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
* https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html
* https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-linux.html
  * https://docs.docker.com/engine/install/ubuntu/
  * https://docs.brew.sh/Homebrew-on-Linux
* https://micronaut.io/blog/2020-08-31-micronaut-2-aws-lambda.html

* https://medium.com/@spff/auto-deploy-aws-lambda-java8-with-gitlab-ci-34a408b0ebce
* https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html
* `aws lambda update-function-code --function-name $awsFunctionName --zip-file fileb://$buildDir/$zipFile`
  
https://mkyong.com/gradle/gradle-create-a-jar-file-with-dependencies/
https://discuss.gradle.org/t/how-to-include-dependencies-in-jar/19571/17