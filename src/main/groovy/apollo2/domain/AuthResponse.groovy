package apollo2.domain

class AuthResponse {
  String id
  String status
  String merchantRefNum
  String txnTime
  Boolean settleWithAuth
  int amount
  String authCode
  java.lang.Error error

  @Override
  String toString() {
    return "AuthResponse{status=${status}, merchantRefNum=${merchantRefNum}, error=${error}}"
  }
}
