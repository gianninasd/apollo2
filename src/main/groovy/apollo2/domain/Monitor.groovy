package apollo2.domain

/**
 * Contains the status of the apollo2.service
 */
class Monitor {

  public status = "RUNNING"
}
