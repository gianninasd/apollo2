package apollo2
import io.micronaut.core.annotation.Introspected
import io.micronaut.function.aws.MicronautRequestHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Introspected
class BookRequestHandler extends MicronautRequestHandler<Book, BookSaved> {

    private static final Logger log = LoggerFactory.getLogger(BookRequestHandler.class)

    @Override
    BookSaved execute(Book input) {
        println "boo2>> ${this.getApplicationContext()}"
        println "boo2>> ${this.getProperties()}"
        println "boo2>> ${this.getMetaPropertyValues()}"

        log.info("Incoming request from ${this.getApplicationContext()}")

        for ( PropertyValue x in this.getMetaPropertyValues() ) {
            println "${x.getName()} >> ${x.getValue()}"
        }

        BookSaved bookSaved = new BookSaved()
        bookSaved.with {
            name = input.getName()
            isbn = UUID.randomUUID().toString() + "-jim"
        }
        bookSaved
    }
}