package apollo2

import apollo2.domain.AuthResponse
import apollo2.domain.CardRequest
import apollo2.domain.CardResponse
import apollo2.service.AuthProcessor
import io.micronaut.core.annotation.Introspected
import io.micronaut.function.aws.MicronautRequestHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Introspected
class AuthRequestHandler extends MicronautRequestHandler<CardRequest, AuthResponse> {

  private static final Logger log = LoggerFactory.getLogger(AuthRequestHandler.class)
  private AuthProcessor proc = new AuthProcessor()

  @Override
  AuthResponse execute(CardRequest cardRequest) {
    //log.info("Incoming request from ${req.remoteAddress}")

    // generate UUID
    String uuid = UUID.randomUUID().toString()

    log.info("${uuid} Processing ${cardRequest}")

    // TODO validate request
    // run thru simulator
    CardResponse resp = proc.process(uuid, cardRequest)
    log.info("${uuid} Response was ${resp.authResponse}")

    return resp.authResponse
  }
}
